<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="minesweep" version="0.1" date="03/06/2020">
        <Author name="Sullemunk" email="" />
        <Description text="Just some MineSweeper, use /sweep &lt;easy&gt;,&lt;normal&gt; or &lt;hard&gt;" />
		<VersionSettings gameVersion="1.3" />
		<Dependencies>
		<Dependency name="LibSlash" optional="true" />
		</Dependencies>
        <Files>
			<File name="minesweep.lua" />
			<File name="minesweep.xml" />
		</Files>
        <OnInitialize>
            <CallFunction name="minesweep.OnInitialize" />
        </OnInitialize>
		<OnUpdate>
		</OnUpdate>
		<OnShutdown>
		</OnShutdown>
		<SavedVariables>
		</SavedVariables>
    </UiMod>
</ModuleFile> 