minesweep = {}
local total_mines = 0
local fieldX = 0
local fieldY = 0
local minesweep_Color = {[0]=DefaultColor.BLACK,[1]=DefaultColor.WHITE,[2]=DefaultColor.GREEN,[3]=DefaultColor.BLUE,[4]=DefaultColor.PURPLE,[5]=DefaultColor.GOLD,[6]=DefaultColor.RED,[7]=DefaultColor.TEAL,[8]=DefaultColor.SILVER}

function minesweep.OnInitialize()
	LibSlash.RegisterSlashCmd("sweep", function(input) minesweep.Command(input) end)
end

function minesweep.MakeField(Width,Height,Mines)
	if DoesWindowExist("MineSweepWindow") then 
		DestroyWindow("MineSweepWindow") 
	end
	
	CreateWindow("MineSweepWindow", true)

fieldX = Width
fieldY = Height
total_mines = Mines
--generate and reset the playfield
WindowSetDimensions("MineSweepWindow",10+(fieldX*28),45+(fieldY*28))
minesweep.field = {}
minesweep.IdToCord = {}
minesweep.GameOver = false
minesweep.CellsToClear = (fieldX*fieldY)-total_mines
LabelSetText("MineSweepWindowTitleBarText",towstring(minesweep.CellsToClear))

local CellNumber = 1
	for x=1,fieldX do		
		minesweep.field[x] = {}
		for y=1,fieldY do	
		local BoxName = "MSW"..x.."_"..y
			if not DoesWindowExist(BoxName) then CreateWindowFromTemplate(BoxName, "minesweep_Box", "MineSweepWindow") end
			DynamicImageSetTextureSlice(BoxName.."Icon","")
			WindowSetTintColor(BoxName.."Frame",200,200,200)
			WindowSetTintColor(BoxName.."BG",55,55,155) 			
			LabelSetText(BoxName.."Text",L"")			
			WindowSetShowing(BoxName.."Icon",false)			
			WindowSetId( BoxName, CellNumber )
			WindowClearAnchors( BoxName )
			WindowAddAnchor(BoxName , "topleft", "MineSweepWindow", "topleft",5+((x-1)*28) ,35+((y-1)*28))
			minesweep.field[x][y] = {HasMine = false,Cell_Number = CellNumber,field=y,Tagged = false,Revealed=false,IsEmpty=false}
			minesweep.IdToCord[CellNumber] = {Cell_x = x,Cell_y = y}
			CellNumber = CellNumber+1
		end
	end
--Setting Mines	
    local mines_placed = 0
    while mines_placed < total_mines do
	local RandomizeX,RandomizeY = math.random(fieldX), math.random(fieldY)
		if minesweep.field[RandomizeX][RandomizeY].HasMine == false then
			minesweep.field[RandomizeX][RandomizeY].HasMine = true
			DynamicImageSetTextureSlice("MSW"..RandomizeX.."_"..RandomizeY.."Icon","BombDestruction")			
			mines_placed = mines_placed+1
		end   
    end	
--setting up Cell neighburs in arrays		
for k,v in pairs(minesweep.field) do
	for a,b in pairs(v) do
		minesweep.field[k][a].Neighbours,minesweep.field[k][a].Nerby_Mines=minesweep.Check_Neighbours(k,b.field)
	end
end
end

function minesweep.Check_Neighbours(x,y)
	local Cell_Table = {}
	local Nerby_Mines = 0
	local CN = minesweep.field[x][y].Cell_Number
	if x ~= 1 and y ~= 1 then Cell_Table[1]= minesweep.field[x-1][y-1].Cell_Number;if minesweep.field[x-1][y-1].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[1]= 0 end
	if y ~= 1 then Cell_Table[2]= minesweep.field[x][y-1].Cell_Number;if minesweep.field[x][y-1].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[2]= 0 end
	if x ~= fieldX and y ~= 1 then Cell_Table[3]= minesweep.field[x+1][y-1].Cell_Number;if minesweep.field[x+1][y-1].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[3]= 0 end
	if x ~= 1 then Cell_Table[4]= minesweep.field[x-1][y].Cell_Number;if minesweep.field[x-1][y].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[4]= 0 end
		Cell_Table[5]= minesweep.field[x][y].Cell_Number
	if x ~= fieldX then Cell_Table[6]= minesweep.field[x+1][y].Cell_Number;if minesweep.field[x+1][y].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[6]= 0 end
	if x ~= 1 and y ~= fieldY then Cell_Table[7]= minesweep.field[x-1][y+1].Cell_Number;if minesweep.field[x-1][y+1].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[7]= 0 end
	if y ~= fieldY then Cell_Table[8]= minesweep.field[x][y+1].Cell_Number;if minesweep.field[x][y+1].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[8]= 0 end
	if x ~= fieldX and y ~= fieldY then Cell_Table[9]= minesweep.field[x+1][y+1].Cell_Number;if minesweep.field[x+1][y+1].HasMine then Nerby_Mines = Nerby_Mines+1 end else Cell_Table[9]= 0 end
	return Cell_Table,Nerby_Mines
end

function minesweep.LButtonUp()
if minesweep.GameOver then return end
	local ContextID = WindowGetId( SystemData.ActiveWindow.name )
	local TableData = minesweep.field[minesweep.IdToCord[ContextID].Cell_x][minesweep.IdToCord[ContextID].Cell_y]
	local WindowName = "MSW"..minesweep.IdToCord[ContextID].Cell_x.."_"..minesweep.IdToCord[ContextID].Cell_y

	if TableData.Tagged == false then
		if TableData.HasMine == true then	--Stepped on a mine; Game Over
			WindowSetShowing(WindowName.."Icon",true)
			WindowSetTintColor(WindowName.."Frame",255,0,0)		
			DynamicImageSetTextureSlice(WindowName.."Icon","BombDestruction") 
			minesweep.GameOver = true
			minesweep.Reveal()						
		else
				if TableData.Nerby_Mines == 0 then WindowSetTintColor(WindowName.."Frame",175,175,255) --No mines nearby, do a 9 grid sweep
				local sweep,_ = minesweep.Check_Neighbours(minesweep.IdToCord[ContextID].Cell_x,minesweep.IdToCord[ContextID].Cell_y)
					for k,v in pairs(sweep) do
						if v ~= 0 then minesweep.ShowCell(minesweep.IdToCord[v].Cell_x,minesweep.IdToCord[v].Cell_y) end
					end
				else
					minesweep.ShowCell(minesweep.IdToCord[ContextID].Cell_x,minesweep.IdToCord[ContextID].Cell_y) --else just a ordinary cell reveal
				end
		end
	end
	LabelSetText("MineSweepWindowTitleBarText",towstring(minesweep.CellsToClear))
end

function minesweep.ShowCell(CellX,CellY)
			local WindowName = "MSW"..CellX.."_"..CellY
			local TableData = minesweep.field[CellX][CellY]
			local color = minesweep_Color[TableData.Nerby_Mines]
			LabelSetText(WindowName.."Text",towstring(TableData.Nerby_Mines))
			LabelSetTextColor(WindowName.."Text",color.r,color.g,color.b)
			WindowSetShowing(WindowName.."Text",TableData.Nerby_Mines>0)
			WindowSetTintColor(WindowName.."BG",175,175,255) 
			if TableData.Nerby_Mines == 0 then WindowSetTintColor(WindowName.."Frame",175,175,255) end
			if minesweep.field[CellX][CellY].Revealed == false then
				minesweep.CellsToClear = minesweep.CellsToClear-1
				minesweep.field[CellX][CellY].Revealed = true
				minesweep.field[CellX][CellY].Tagged = false
				WindowSetShowing(WindowName.."Icon",false)	
				WindowSetTintColor(WindowName.."Frame",200,200,200)
			end
return
end

function minesweep.RButtonUp()
if minesweep.GameOver then return end
	local ContextID = WindowGetId( SystemData.ActiveWindow.name )
	local TableData = minesweep.field[minesweep.IdToCord[ContextID].Cell_x][minesweep.IdToCord[ContextID].Cell_y]
	local WindowName = "MSW"..minesweep.IdToCord[ContextID].Cell_x.."_"..minesweep.IdToCord[ContextID].Cell_y
	
	if TableData.Revealed == false then
		TableData.Tagged = not TableData.Tagged		 
		WindowSetShowing(WindowName.."Icon",TableData.Tagged)	 
		DynamicImageSetTextureSlice(WindowName.."Icon","GuildStandard") 
		if TableData.Tagged then
			WindowSetTintColor(WindowName.."Frame",255,255,0)
		else
			WindowSetTintColor(WindowName.."Frame",200,200,200)
		end
	end
end

function minesweep.Reveal()
for k,v in pairs(minesweep.field) do
	for a,b in pairs(v) do
	if b.HasMine == true then 
		WindowSetShowing("MSW"..k.."_"..a.."Icon",true)	 
	end
	end
end
end

function minesweep.Close()
DestroyWindow("MineSweepWindow") 
end

function minesweep.Command(input)
if input == "easy" then minesweep.MakeField(8,8,10) 
elseif input == "normal" then minesweep.MakeField(20,20,65) 
elseif input == "hard" then minesweep.MakeField(30,16,100) 
end
end